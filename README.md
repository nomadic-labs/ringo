# Ringo, Aches, Aches-lwt

Caches (bounded-size, key-value or simple-value stores) and other bounded-size
collections.

## Ringo

Ringo is a library for bounded-size collections. These collections are
parametrised by size limit. When elements are inserted beyond the size limit,
another element is removed to make room.

## Aches

Aches is a library for caches. It provides Value-Caches (in the `Vache` module)
as well as Resource-Caches (in the `Rache` module). Unlike value caches,
resources caches have a built-in clean-up mechanism and a clear ownership
mechanism (to keep track of who is responsible for cleanning-up a resource).

## Aches-lwt

Aches-lwt is a library for caches of Lwt-promises. It provides
Lwt-promise-Caches (in the `Lache` module) which have a dedicated interface to
avoid some common race-conditions with concurrency and caches. It also provides
opinionated caches for error-management and automated cancelling of promises.

# Documentation

The documentation is available in the `mli` files of the library.
