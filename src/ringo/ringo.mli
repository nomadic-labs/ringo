(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2022 Nomadic Labs, <contact@nomadic-labs.com>               *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** {1 Ringo} *)

(** Ringo is a library for bounded-length collections. *)

(** {2 Generic collections} *)

module type UNBOXED_COLLECTION = Sigs.UNBOXED_COLLECTION

(** [Ring] is a potentially useful module that is used internally to manage
    bounded, FIFO collections of items. The documentation is available in
    {!UNBOXED_COLLECTION}.

    It is implemented as an abstraction over an array.

    See {!Dll} for a comparison. *)
module Ring : UNBOXED_COLLECTION

(** [Dll] is a potentially useful module that is used internally to manage
    bounded, LRU collections of items. The documentation is available in
    {!UNBOXED_COLLECTION}.

    It is implemented as an abstraction over a doubly-linked list.

    The implementations of [Ring] and [Dll] are functionally indistinguishable.
    However, their memory consumptions differ. On the one hand, with a [Ring],
    the whole structure is allocated in its entirety as soon as a single element
    is [add]ed. Afterwards, there are no more allocations.

    On the other hand, with a [Dll], cells holding the [add]ed values are
    allocated on a by-need basis. Inserting a supernumerary element renders one
    single cell garbage-collectible.

    In other words, [Ring] allocates a bigger chunk of data in one go but is
    stable afterwards, whereas [Dll] allocates small chunks of data one-by-one.
    *)
module Dll : UNBOXED_COLLECTION

module type UNBOXED_WEIGHTED_COLLECTION = Sigs.UNBOXED_WEIGHTED_COLLECTION

(** [Weighted_Dll] is similar to [Dll] but comes with a notion of data
    weight as capacity instead of being based on the number of
    elements. The documentation is available in
    {!UNBOXED_WEIGHTED_COLLECTION}.

    By contrast to [Dll], elements require their weight to be provided
    in order to be inserted in the doubly-linked list. *)
module Weighted_Dll : UNBOXED_WEIGHTED_COLLECTION

(** {2 Cache-oriented collections} *)

module type COLLECTION = Sigs.COLLECTION

(** [LRU_Collection] is a [COLLECTION] meant to be used as a building block in a
    cache: specifically a cache with a {e Least-Recently Used} replacement
    policy.

    Attempting to insert a supernumerary element causes the least-recently
    promoted element to be removed. Thus, the cache implementation simply
    needs to use [promote_read] and [promote_write] according to cache accesses
    to obtain an LRU replacement policy.

    In addition, accounting in [LRU_Collection] is precise: [remove]d elements
    never count towards the length-limit. *)
module LRU_Collection: Sigs.COLLECTION

(** [FIFO_Sloppy_Collection] is a [COLLECTION] meant to be used as a building
    block in a cache: specifically a cache with a {e First In First Out}
    replacement policy.

    Attempting to insert a supernumerary element causes the oldest
    least-recently write-promoted (i.e., the least-recently added) element to be
    removed. Thus, the cache implementation simply needs to use [promote_read]
    and [promote_write] according to cache accesses to obtain a FIFO replacement
    policy.

    In addition, accounting in [FIFO_Sloppy_Collection] is sloppy: [remove]d
    elements count towards the size-bound until enough elements have been
    inserted that the removed elements disappears. *)
module FIFO_Sloppy_Collection: Sigs.COLLECTION

(** [FIFO_Precise_Collection] is a [COLLECTION] meant to be used as a building
    block in a cache: specifically a cache with a {e First In First Out}
    replacement policy.

    Attempting to insert a supernumerary element causes the oldest
    least-recently write-promoted (i.e., the least-recently added) element to be
    removed. Thus, the cache implementation simply needs to use [promote_read]
    and [promote_write] according to cache accesses to obtain a FIFO replacement
    policy.

    In addition, accounting in [FIFO_Precise_Collection] is precise: [remove]d
    elements never count towards the length-limit. *)
module FIFO_Precise_Collection: Sigs.COLLECTION

module type WEIGHTED_COLLECTION = Sigs.WEIGHTED_COLLECTION

(** [Weighted_LRU_Collection] is similar to {!LRU_Collection} but comes
    with a notion of data weight as capacity instead of being based on
    the number of elements.

    This differentiation allows building {e memory-bounded} cache
    using a {e Least-Recently Used} replacement policy.

    Attempting to insert an element causes the least-recently promoted
    elements to be removed. The cache will remove as many elements as
    required so that the new element fits. *)
module Weighted_LRU_Collection : WEIGHTED_COLLECTION
