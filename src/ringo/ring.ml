(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2018 Dynamic Ledger Solutions, Inc. <contact@tezos.com>     *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

type 'a raw =
  | Empty of int
  | Inited of {
      data : 'a array;
      mutable size : int (* number of element of [data] which are part of the ring *);
      mutable pos : int (* offset of the newest element of the ring *)
  }

type 'a t = 'a raw ref

let create capacity =
  if capacity <= 0 then invalid_arg "Ring.create: capacity must be positive"
  else ref (Empty capacity)

let capacity r =
  match !r with
  | Empty capacity -> capacity
  | Inited {data; _} -> Array.length data

let length r =
  match !r with
  | Empty _ -> 0
  | Inited {data; size; pos = _} ->
      assert (size <= Array.length data) ;
      size

let add r v =
  match !r with
  | Empty size -> r := Inited {data = Array.make size v; size = 1; pos = 0}
  | Inited ({data; size; pos} as s) ->
      let capacity = Array.length data in
      let new_size = min (size + 1) capacity in
      let new_pos = (pos + 1) mod capacity in
      s.data.(new_pos) <- v ;
      s.pos <- new_pos ;
      s.size <- new_size

let add_and_return_erased r v =
  let replaced =
    match !r with
    | Empty _ -> None
    | Inited s ->
        let capacity = Array.length s.data in
        if s.size = capacity then Some s.data.((s.pos + 1) mod capacity)
        else None
  in
  add r v ;
  replaced

let clear r =
  match !r with
  | Empty _ -> ()
  | Inited {data; _} -> r := Empty (Array.length data)

let add_list r l = List.iter (add r) l

let fold r ~init ~f =
  match !r with
  | Empty _ -> init
  | Inited {data; size; pos} ->
      let acc = ref init in
      let capacity = Array.length data in
      for i = 0 to size - 1 do
        acc := f !acc data.((pos - i + capacity) mod capacity)
      done ;
      !acc

let fold_oldest_first r ~init ~f =
  match !r with
  | Empty _ -> init
  | Inited {data; size; pos} ->
      let acc = ref init in
      let capacity = Array.length data in
      for i = size - 1 downto 0 do
        acc := f !acc data.((pos - i + capacity) mod capacity)
      done ;
      !acc

let newest_element t =
  match !t with Empty _ -> None | Inited {data; pos; _} -> Some data.(pos)

let oldest_element t =
  match !t with
  | Empty _ -> None
  | Inited {data; size; pos; _} ->
      let capacity = Array.length data in
      let oldest_pos = (pos - (size - 1) + capacity) mod capacity in
      Some data.(oldest_pos)

let elements t = fold t ~init:[] ~f:(fun acc elt -> elt :: acc)

let remove_newest r =
  match !r with
  | Empty _ -> None
  | Inited {data = _; size = 0; pos = _} -> assert false
  | Inited {data; size = 1; pos} ->
      r := Empty (Array.length data);
      Some data.(pos)
  | Inited ({data; size; pos} as s) ->
      let capacity = Array.length data in
      let removed = data.(pos) in
      s.pos <- (pos - 1 + capacity) mod capacity ;
      data.(pos) <- data.(s.pos) ;
      s.size <- size - 1 ;
      Some removed

let remove_oldest r =
  match !r with
  | Empty _ -> None
  | Inited {data = _; size = 0; pos = _} -> assert false
  | Inited {data; size = 1; pos} ->
      r := Empty (Array.length data);
      Some data.(pos)
  | Inited ({data; size; pos} as s) ->
      let capacity = Array.length data in
      let removed_pos = (pos - (size - 1) + capacity) mod capacity in
      let removed = data.(removed_pos) in
      data.(removed_pos) <- data.(s.pos) ;
      s.size <- size - 1 ;
      Some removed
