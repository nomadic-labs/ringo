(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2020 Nomadic Labs, <contact@nomadic-labs.com>               *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

type 'a node = {
   data: 'a;
   mutable newer: 'a node option;
   mutable older: 'a node option;
}

let data {data; _} = data

type 'a tt =
  | Empty of {capacity: int}
  | Inited of {
      capacity: int;
      mutable size: int;
      mutable newest: 'a node;
      mutable oldest: 'a node;
   }

type 'a t = 'a tt ref

let create capacity =
  if capacity <= 0 then
    raise (Invalid_argument "Ringo.Dll.create: negative or null capacity")
  else
    ref (Empty {capacity})

let capacity dll =
  match !dll with
  | Empty {capacity}
  | Inited {capacity; _} -> capacity

let length dll =
  match !dll with
  | Empty _ -> 0
  | Inited {size; _} -> size

let add_and_return_erased dll data =
  match !dll with
  | Empty {capacity} ->
      let node = {data; newer = None; older = None} in
      dll := Inited {capacity; size = 1; newest = node; oldest = node;};
      (node, None)
  | Inited ({capacity = 1; size; newest; oldest;} as dll) ->
      assert (size = 1);
      assert (newest == oldest);
      assert (newest.older = None);
      assert (newest.newer = None);
      assert (oldest.older = None);
      assert (oldest.newer = None);
      let pops = oldest in
      let node = {data; newer = None; older = None} in
      dll.newest <- node;
      dll.oldest <- node;
      (node, Some pops.data)
  | Inited ({capacity; size; newest; oldest;} as dll) ->
      assert (newest.newer = None);
      assert (oldest.older = None);
      if size < capacity then begin
        let node = {data; newer = None; older = Some newest} in
        newest.newer <- Some node;
        dll.newest <- node;
        dll.size <- succ dll.size;
        (node, None)
      end else begin
        let pops = oldest in
        ( match oldest.newer with
        | Some new_oldest ->
            dll.oldest <- new_oldest;
            new_oldest.older <- None
        | None ->
            (* This requires
               (1) to have a single element,
               (2) to have reached capacity, and
               (3) to have a capacity > 1 *)
              assert false );
        pops.newer <- None;
        pops.older <- None;
        let node = {data; newer = None; older = Some dll.newest} in
        newest.newer <- Some node;
        dll.newest <- node;
        (node, Some pops.data)
      end

let add dll data = fst @@ add_and_return_erased dll data

let add_list dll l =
  let capacity = capacity dll in
  let length = List.length l in
  if length < capacity then begin
    List.map (add dll) l |> List.rev
  end else begin
    List.fold_left
      (fun (index, acc) x ->
        if index < length - capacity then
          (index + 1, acc)
        else
          (index + 1, add dll x:: acc))
      (0, [])
      l
    |> fun (_, acc) -> List.rev acc
  end

let clear dll =
  match !dll with
  | Empty _ -> ()
  | Inited {capacity; _} -> dll := Empty {capacity}

let rec fold_node f acc node =
  let acc = f acc node in
  match node.older with
  | None -> acc
  | Some older -> fold_node f acc older

let fold dll ~init ~f =
  match !dll with
  | Empty _ -> init
  | Inited {newest; _} -> fold_node f init newest

let rec fold_node_oldest_first f acc node =
  let acc = f acc node in
  match node.newer with
  | None -> acc
  | Some newer -> fold_node_oldest_first f acc newer

let fold_oldest_first dll ~init ~f =
  match !dll with
  | Empty _ -> init
  | Inited {oldest; _} -> fold_node_oldest_first f init oldest

let oldest_element dll =
  match !dll with
  | Empty _ -> None
  | Inited {oldest; _} -> Some oldest

let newest_element dll =
  match !dll with
  | Empty _ -> None
  | Inited {newest; _} -> Some newest

let elements t = fold t ~init:[] ~f:(fun acc elt -> elt:: acc)

let elements_data t = fold t ~init:[] ~f:(fun acc elt -> elt.data:: acc)

let remove c node =
  match !c with
  | Empty _ -> assert false
  | Inited dll ->
      begin match (node.newer, node.older) with
      | (None, None) ->
          assert (node == dll.newest);
          assert (node == dll.oldest);
          assert (dll.size = 1);
          c := Empty {capacity = dll.capacity}
      | (None, Some older) ->
          older.newer <- None;
          dll.newest <- older;
      | (Some newer, None) ->
          newer.older <- None;
          dll.oldest <- newer
      | (Some newer, Some older) ->
          newer.older <- node.older;
          older.newer <- node.newer 
      end;
      node.newer <- None;
      node.older <- None;
      dll.size <- pred dll.size

let remove_newest c =
  match !c with
  | Empty _ -> None
  | Inited {size = 0; _} -> assert false
  | Inited {capacity; size; newest; oldest} ->
      assert (newest.newer = None) ;
      (match newest.older with
      | None ->
          (* [newest] is the only element *)
          assert (newest == oldest);
          c := Empty {capacity}
      | Some older ->
          newest.older <- None ;
          older.newer <- None ;
          c := Inited {capacity; size = size - 1; newest = older; oldest}) ;
      Some newest

let remove_oldest c =
  match !c with
  | Empty _ -> None
  | Inited {size = 0; _} -> assert false
  | Inited {capacity; size; newest; oldest} ->
      assert (oldest.older = None) ;
      (match oldest.newer with
      | None ->
          (* [oldest] is the only element *)
          assert (oldest == newest);
          c := Empty {capacity}
      | Some newer ->
          oldest.newer <- None ;
          newer.older <- None ;
          c := Inited {capacity; size = size - 1; newest; oldest = newer}) ;
      Some oldest

let promote dll node =
  match !dll with
  | Empty _ -> assert false
  | Inited dll ->
      if dll.newest == node then
        ()
      else begin
        let prev_newest = dll.newest in
        (* newest, promote neighbors *)
        begin match (node.newer, node.older) with
        | (None, None) -> assert false
        | (None, Some _next) -> assert false
        | (Some newer, None) ->
            newer.older <- None;
            dll.oldest <- newer
        | (Some newer, Some older) ->
            newer.older <- node.older;
            older.newer <- node.newer
        end;
        (* promote node to newest *)
        prev_newest.newer <- Some node;
        node.newer <- None;
        node.older <- Some dll.newest;
        dll.newest <- node
     end
