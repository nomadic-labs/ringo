(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2022 Nomadic Labs, <contact@nomadic-labs.com>               *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** Aches: a variety of caches

    Aches is a library that provides caches (limited-size collections with
    automatic discarding of supernumerary elements) for a variety of uses.

    Aches (more nerdily named [*ache] where the [*] stands for globbing)
    provides the following modules:

    {!Vache}: Value cache. That is caches for in-memory values. These caches
    come in different kinds: maps (key-value caches) and sets (value-only
    caches), and with different policies: FIFO/LRU, Precise/Sloppy size
    accounting, Strong/Weak GC handling.

    {!Rache}: Resource cache. That is caches for resources (file descriptors,
    network connections, etc.) which need some clean-up beyond simple garbage
    collection. These caches maintain a strict ownership discipline which you
    must also follow: resources are either owned by you or by the cache, the
    owner is responsible for the clean-up.
 *)

(** Value caches *)
module Vache = Vache

(** Resource caches *)
module Rache = Rache
