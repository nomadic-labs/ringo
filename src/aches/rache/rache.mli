(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2022 Nomadic Labs, <contact@nomadic-labs.com>               *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** {1 Rache} *)

(** Rache is a library for resource caches.

    More specifically, Rache provides modules implementing caches for resources.
    These caches are parametrised by cache-policies: replacement policy and
    maximum size. The caches also provide a clean-up mechanism to deal with
    resource tear-down. *)

(** {2 Caches} *)

(** [TRANSFER] are caches in which resources can be put and from which
    resources can be taken. More precisely, caches where the ownership of the
    resources (the responsibility to clean them up) can be transferred into and
    out of the cache.

    Check the documentation of the interface for more details. *)
module type TRANSFER = Sigs.TRANSFER

(** [BORROW] are caches in which resources can be borrowed but never
    transferred. In other words, the cache retains ownership of all resources.

    Check the documentation of the interface for more details. *)
module type BORROW = Sigs.BORROW

(** All caches of Rache have either the {!TRANSFER} interface (for caches
    with transfer of ownership) or the {!BORROW} interface (for caches
    with borrowing of resources). Their behaviour can be tweaked by the
    parameters below. *)

(** {2 Cache policies} *)

(** [REPLACEMENT] is for defining the replacement policy of a cache. [LRU] is
    for "Least Recently Used", meaning that when a supernumerary item is
    inserted in the cache, the least recently used item is removed to make room.
    [FIFO] is for "First-In, First-Out" meaning that when a supernumerary item
    is inserted in the cache, the oldest inserted element is removed to make
    room.

    Resources which are removed from resource caches are cleaned-up by the
    cache. Do follow the documentation carefully (e.g., avoid keeping references
    to resources you do not own) to avoid issues related to this. *)
module type REPLACEMENT
module LRU : REPLACEMENT
module FIFO : REPLACEMENT

(** {2 Cache instantiating} *)

(** [Transfer(R)(H)] is a [TRANSFER] indexed by [H] and govern by the
    replacement policy [R]. *)
module Transfer
  (R: REPLACEMENT)
  (H: Hashtbl.HashedType)
: TRANSFER with type key = H.t

(** [Borrow(R)(H)] is a [BORROW] indexed by [H] and govern by the
    replacement policy [R]. *)
module Borrow
  (R: REPLACEMENT)
  (H: Hashtbl.HashedType)
: BORROW with type key = H.t

(** [EmptyTransferMap(H)] is a transfer-map module but it only supports the
    empty map: a map with zero elements.

    The [create] function ignores its size-limit parameter: the size limit is
    hardcoded to [0].

    Note that all policies are equivalent in the case of an empty transfer-map.
    This is why the empty transfer-map does not require the user to specify any
    policy. *)
module EmptyTransferMap
  (H: Hashtbl.HashedType)
: TRANSFER with type key = H.t

(** [SingletonTransferMap(H)] is a transfer-map module but it only supports
    singleton maps: maps with at most one element.

    The [create] function ignores its size-limit parameter: the size limit is
    hardcoded to [1].

    Note that all policies are equivalent in the case of a singleton
    transfer-map. This is why the singleton transfer-map does not require the
    user to specify any policy. *)
module SingletonTransferMap
  (H: Hashtbl.HashedType)
: TRANSFER with type key = H.t

(** [EmptyBorrowMap(H)] is a borrow-map module but it only supports the
    empty map: a map with zero elements.

    The [create] function ignores its size-limit parameter: the size limit is
    hardcoded to [0].

    Note that all policies are equivalent in the case of an empty borrow-map.
    This is why the empty borrow-map does not require the user to specify any
    policy. *)
module EmptyBorrowMap
  (H: Hashtbl.HashedType)
: BORROW with type key = H.t

(** [SingletonBorrowMap(H)] is a borrow-map module but it only supports
    singleton maps: maps with at most one element.

    The [create] function ignores its size-limit parameter: the size limit is
    hardcoded to [1].

    Note that all policies are equivalent in the case of a singleton
    borrow-map. This is why the singleton borrow-map does not require the
    user to specify any policy. *)
module SingletonBorrowMap
  (H: Hashtbl.HashedType)
: BORROW with type key = H.t
