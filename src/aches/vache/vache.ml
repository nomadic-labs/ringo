(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2022 Nomadic Labs, <contact@nomadic-labs.com>               *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

module type MAP = Sigs.MAP
module type SET = Sigs.SET

module type REPLACEMENT_AND_ACCOUNTING = Ringo.COLLECTION
module LRU_Precise : REPLACEMENT_AND_ACCOUNTING = Ringo.LRU_Collection
module LRU_Sloppy : REPLACEMENT_AND_ACCOUNTING = Ringo.LRU_Collection
module FIFO_Precise : REPLACEMENT_AND_ACCOUNTING = Ringo.FIFO_Precise_Collection
module FIFO_Sloppy : REPLACEMENT_AND_ACCOUNTING = Ringo.FIFO_Sloppy_Collection

module type OVERFLOW = Sigs.TABLER
module Strong : OVERFLOW = Strong_tabler.Make
module Weak : OVERFLOW = Weak_tabler.Make

include Functors
include Sized
