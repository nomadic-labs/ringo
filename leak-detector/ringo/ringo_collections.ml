(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2022 Nomadic Labs, <contact@nomadic-labs.com>               *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

let main ~seed ?(size = 50) ?(repeat = 3000) () (name, (module C : Ringo.COLLECTION)) =
  let prng = Random.State.make [| seed |] in
  if size < 3 then raise (Invalid_argument "main: size too low");
  if repeat <= 0 then raise (Invalid_argument "main: repeat too low");
  Printf.printf "leak-detector: ringo (%s) (seed: %d) (size: %d) (repeat: %d)…" name seed size repeat;
  let c = C.create size in
  let u = C.add c (Bytes.create 1) in
  let v = C.add c (Bytes.create 1) in
  let rec loop garbage max_reachable_words u v repeat =
    assert (C.length c <= size);
    let reachable_words = Obj.reachable_words (Obj.repr c) in
    assert (reachable_words < (size * 10 + 5)); (* magic constant from observations *)
    let max_reachable_words = max max_reachable_words reachable_words in
    if repeat <= 0 then
      max_reachable_words
    else
      match Random.State.int prng 8 with
      | 0 | 1 | 2 ->
          let w = C.add c (Bytes.create 1) in
          loop garbage max_reachable_words v w (repeat - 1)
      | 3 ->
          let (w, erased) = C.add_and_return_erased c (Bytes.create 1) in
          loop (erased :: garbage) max_reachable_words v w (repeat - 1)
      | 4 ->
          let w = C.add c (Bytes.create 2) in
          loop garbage max_reachable_words v w (repeat - 1)
      | 5 ->
          C.promote_read c u;
          let w = C.add c (Bytes.create 1) in
          loop garbage max_reachable_words v w (repeat - 1)
      | 6 ->
          C.promote_write c v;
          let w = C.add c (Bytes.create 1) in
          loop garbage max_reachable_words u w (repeat - 1)
      | 7 ->
          C.promote_read c v;
          let w = C.add c (Bytes.create 1) in
          loop garbage max_reachable_words u w (repeat - 1)
      | _ -> assert false
  in
  match loop [] 0 u v repeat with
  | exception Assert_failure _ ->
      Printf.printf " LEAKY!\n"; exit 1
  | exception exc ->
      Printf.printf " error (%s)\n" (Printexc.to_string exc); exit 1
  | max_reachable_words ->
      Printf.printf " ok (maximum reachable-words: %d)\n" max_reachable_words

let () =
  let seed = Option.map int_of_string (Sys.getenv_opt "LEAKSEED") in
  let seed = match seed with
    | Some seed -> seed
    | None ->
        let seederprng = Random.State.make_self_init () in
        Random.State.int seederprng 0xff_ff_ff
  in
  let size = Option.map int_of_string (Sys.getenv_opt "LEAKSIZE") in
  let repeat = Option.map int_of_string (Sys.getenv_opt "LEAKREPEAT") in
  let collections : (string * (module Ringo.COLLECTION)) list =
    match Sys.getenv_opt "LEAKCOLLECTIONS" with
    | None | Some "all" ->
        [
          ("LRU", (module Ringo.LRU_Collection));
          ("FIFO(sloppy)", (module Ringo.FIFO_Sloppy_Collection));
          ("FIFO(precise)", (module Ringo.FIFO_Precise_Collection));
        ]
    | Some "LRU" -> [ ("LRU", (module Ringo.LRU_Collection)); ]
    | Some "FIFO" ->
        [
          ("FIFO(sloppy)", (module Ringo.FIFO_Sloppy_Collection));
          ("FIFO(precise)", (module Ringo.FIFO_Precise_Collection));
        ]
    | _ -> raise (Invalid_argument "main: invalid LEAKCOLLECTIONS (expected all, LRU, or FIFO)")
  in
  List.iter (main ~seed ?size ?repeat ()) collections

