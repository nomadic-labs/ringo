(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2024 Nomadic Labs, <contact@nomadic-labs.com>               *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(* A small PBT where we check the weight limit after every operation on the
   weighted collection.

   The simple system under test is a weighted dll of strings where the weight is
   the length of the string. This allows us to reconstitute the weight from the
   data-content of the collection to compare with what the weighted-dll
   implementation reports. *)

let test prng (module Collection : Ringo.WEIGHTED_COLLECTION) =

  let weight_limit = 111 in

  let c = Collection.create weight_limit in

  let step () =
    let action = Random.State.int prng 100 in
    match action with
    | 0 | 1 ->
        let _ : string Collection.node option = Collection.remove_oldest c in
        ()
    | 2 | 3 ->
        let _ : string Collection.node option = Collection.remove_newest c in
        ()
    | 4 | 5 ->
        let n : string Collection.node option = Collection.oldest_element c in
        Option.iter (Collection.promote c) n;
        ignore n;
        ()
    | 6 -> (* we test less because it's a noop *)
        let n : string Collection.node option = Collection.newest_element c in
        Option.iter (Collection.promote c) n;
        ignore n;
        ()
    | 8 -> (* we test less becaues it's simple *)
        let () = Collection.clear c in
        ()
    | _ ->
        let s = String.make (Random.State.int prng 128) '.' in
        let w = String.length s in
        let _ = Collection.add c (s, w) in
        ()
  in

  let rec drive n =
    if n > 0 then begin
      step ();
      let (folded_length, folded_weight) =
        Collection.fold
          c
          ~init:(0, 0)
          ~f:(fun (l, w) n -> (l+1, w + String.length (Collection.data n)))
      in
      assert (folded_weight = Collection.total_weight c);
      assert (folded_weight <= weight_limit);
      assert (folded_length = Collection.length c);
      drive (n - 1)
    end
  in

  drive 10000;

  ()

(* PRNG initialisation with some reproducibility *)
let seed =
  let seed_var = "RINGO_TEST_PRNG_SEED" in
  match Sys.getenv_opt seed_var with
  | Some n -> int_of_string n
  | None ->
      let () = Random.self_init () in
      let seed = Random.int (1 lsl 29) in
      Printf.printf "%s=%d\n%!" seed_var seed;
      seed

let () =
  let prng = Random.State.make [|seed|] in
  test prng (module Ringo.Weighted_LRU_Collection)
