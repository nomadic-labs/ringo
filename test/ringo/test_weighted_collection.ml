(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2024 Nomadic Labs, <contact@nomadic-labs.com>               *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

let test_weighted_collection (module Collection : Ringo.WEIGHTED_COLLECTION) =
  let eq_box b1 b2 = Collection.data b1 = Collection.data b2 in
  let eq_box_list l1 l2 = List.for_all2 eq_box l1 l2 in
  let eq_node_list l1 l2 =
    List.for_all2 (fun l r -> Collection.data l = r) l1 l2
  in

  let c = Collection.create 5 in

  let () = assert (eq_box_list [] @@ Collection.elements c) in
  let () = assert (Collection.length c = 0) in

  let add_assert c x =
    match Collection.add c x with None -> assert false | Some n -> n
  in

  let node_00 = add_assert c ("00", 1) in
  let () = assert (Collection.data node_00 = "00") in
  let () = assert (Collection.weight node_00 = 1) in
  let () = assert (Collection.total_weight c = 1) in
  let () = assert (eq_box_list [node_00] @@ Collection.elements c) in
  let () = assert (Collection.length c = 1) in

  let node_01 = add_assert c ("01", 0) in
  let () = assert (Collection.data node_01 = "01") in
  let () = assert (Collection.weight node_01 = 0) in
  let () = assert (Collection.total_weight c = 1) in
  let () = assert (eq_box_list [node_00; node_01] @@ Collection.elements c) in
  let () = assert (Collection.length c = 2) in

  assert (Collection.add c ("too_big", 6) = None) ;
  let () = assert (Collection.total_weight c = 1) in
  let () = assert (eq_box_list [node_00; node_01] @@ Collection.elements c) in

  let node_02, erased_nodes =
    match Collection.add_and_return_erased c ("02", 5) with
    | None, _ | _, [] -> assert false
    | Some node_02, l -> (node_02, l)
  in
  let () = assert (Collection.data node_02 = "02") in
  let () = assert (Collection.weight node_02 = 5) in
  let () = assert (Collection.total_weight c = 5) in
  let () = assert (eq_box_list [node_01; node_02] @@ Collection.elements c) in
  let () = assert (Collection.length c = 2) in
  let () = assert (eq_node_list [node_00] erased_nodes) in

  let node_03, erased_nodes =
    match Collection.add_and_return_erased c ("03", 1) with
    | None, _ | _, [] -> assert false
    | Some node_03, l -> (node_03, l)
  in
  let () = assert (Collection.data node_03 = "03") in
  let () = assert (Collection.weight node_03 = 1) in
  let () = assert (Collection.total_weight c = 1) in
  let () = assert (eq_box_list [node_03] @@ Collection.elements c) in
  let () = assert (Collection.length c = 1) in
  let () = assert (eq_node_list [node_01; node_02] erased_nodes) in

  let () = Collection.clear c in

  let () = assert (Collection.add_list c [] = []) in
  let added_elts =
    Collection.add_list c [("a", 5); ("b", 4); ("c", 3); ("d", 2); ("e", 1)]
  in
  let () = assert (eq_node_list added_elts ["d"; "e"]) in
  let () = assert (Collection.total_weight c = 3) in

  let added_elts = Collection.add_list c [("f", 1); ("g", 1)] in
  let () = assert (eq_node_list added_elts ["f"; "g"]) in
  let () = assert (Collection.total_weight c = 5) in

  let added_elts =
    Collection.add_list
      c
      [("e", 1); ("d", 2); ("c", 3); ("b", 4); ("a", 5); ("z", 6)]
  in
  let () = assert (eq_node_list added_elts ["a"]) in
  let () = assert (Collection.total_weight c = 5) in

  let added_elts =
    Collection.add_list c [("e", 1); ("d", 2); ("c", 3); ("b", 4)]
  in
  let () = assert (eq_node_list added_elts ["e"; "b"]) in
  let () = assert (Collection.total_weight c = 5) in

  let e_node, b_node =
    match added_elts with [e; b] -> (e, b) | _ -> assert false
  in
  let () = Collection.remove c b_node in
  let () = assert (Collection.total_weight c = 1) in

  let b_node = add_assert c ("b", 4) in
  let () = assert (eq_box_list [e_node; b_node] @@ Collection.elements c) in
  let () = Collection.promote c e_node in
  let () = assert (eq_box_list [b_node; e_node] @@ Collection.elements c) in

  let () =
    assert (
      eq_box_list
        (Collection.fold_oldest_first c ~init:[] ~f:(fun l x -> x :: l))
        (Collection.elements c |> List.rev))
  in

  let () = Collection.clear c in
  let () = assert (Option.is_none (Collection.newest_element c)) in
  let () = assert (Option.is_none (Collection.oldest_element c)) in

  let node_oldest = add_assert c ("oldest", 2) in
  let node_older = add_assert c ("older", 0) in
  let node_gold = add_assert c ("goldylocks", 1) in
  let node_newer = add_assert c ("newer", 1) in
  let node_newest = add_assert c ("newest", 1) in
  let () = assert (Collection.length c = 5) in
  let () = assert (Collection.total_weight c = 5) in
  let () = assert (match Collection.newest_element c with None -> false | Some n -> Collection.data n = "newest") in
  let () = assert (match Collection.oldest_element c with None -> false | Some n -> Collection.data n = "oldest") in
  let () = assert (eq_box_list
    [node_oldest; node_older; node_gold; node_newer; node_newest]
    (Collection.elements c))
  in

  let removed = Collection.remove_oldest c in
  let () = assert (match removed with | None -> assert false | Some n -> Collection.data n = "oldest") in
  let () = assert (Collection.length c = 4) in
  let () = assert (Collection.total_weight c = 3) in
  let () = assert (eq_box_list
    [node_older; node_gold; node_newer; node_newest]
    (Collection.elements c))
  in
  let () = assert (match Collection.newest_element c with None -> false | Some n -> Collection.data n = "newest") in
  let () = assert (match Collection.oldest_element c with None -> false | Some n -> Collection.data n = "older") in

  let removed = Collection.remove_oldest c in
  let () = assert (match removed with | None -> assert false | Some n -> Collection.data n = "older") in
  let () = assert (Collection.length c = 3) in
  let () = assert (Collection.total_weight c = 3) in
  let () = assert (match Collection.newest_element c with None -> false | Some n -> Collection.data n = "newest") in
  let () = assert (match Collection.oldest_element c with None -> false | Some n -> Collection.data n = "goldylocks") in
  let () = assert (eq_box_list
    [node_gold; node_newer; node_newest]
    (Collection.elements c))
  in

  let removed = Collection.remove_newest c in
  let () = assert (match removed with | None -> assert false | Some n -> Collection.data n = "newest") in
  let () = assert (Collection.length c = 2) in
  let () = assert (Collection.total_weight c = 2) in
  let () = assert (match Collection.newest_element c with None -> false | Some n -> Collection.data n = "newer") in
  let () = assert (match Collection.oldest_element c with None -> false | Some n -> Collection.data n = "goldylocks") in
  let () = assert (eq_box_list
    [node_gold; node_newer]
    (Collection.elements c))
  in

  ()

let () = test_weighted_collection (module Ringo.Weighted_LRU_Collection)
