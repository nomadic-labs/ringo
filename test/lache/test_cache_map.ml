(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2020 Nomadic Labs, <contact@nomadic-labs.com>               *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

module H : Hashtbl.HashedType with type t = int = struct
   type t = int
   let equal = (=)
   let hash = Hashtbl.hash
end

let test (module Cache: Aches_lwt.Lache.MAP with type key = int) =
   let c = Cache.create 5 in
   let () = assert (Cache.length c = 0) in
   let () = assert (Cache.take c 0 = None) in

   (* First phase: test put, bind, and take with an already resolved promise *)

   let () = Cache.put c 0 (Lwt.return "zero") in
   let () = assert (Cache.length c = 1) in

   let z = Cache.bind c 0 Lwt.return in
   let z = Option.get z in
   let () =
       match Lwt.state z with
       | Lwt.Return "zero" -> ()
       | _ -> assert false
   in
   let () = assert (Cache.length c = 1) in

   let z = Cache.take c 0 in
   let z = Option.get z in
   let () =
       match Lwt.state z with
       | Lwt.Return "zero" -> ()
       | _ -> assert false
   in
   let () = assert (Cache.length c = 0) in

   (* Second phase: test put, bind, and take with a yet-to-be-resolved promise *)

   let (p,u) = Lwt.task () in
   let () = Cache.put c 1 p in
   let () = assert (Cache.length c = 1) in

   let z = Cache.take c 1 in
   let z = Option.get z in
   let () =
       match Lwt.state z with
       | Lwt.Sleep -> ()
       | _ -> assert false
   in
   let () = assert (Cache.length c = 0) in
   let () = Cache.put c 1 p in

   let z = Cache.bind c 1 Lwt.return in
   let z = Option.get z in
   let () =
       match Lwt.state z with
       | Lwt.Sleep -> ()
       | _ -> assert false
   in
   let () = assert (Cache.length c = 1) in
   let () = Lwt.wakeup u "one" in
   let () =
       match Lwt.state z with
       | Lwt.Return "one" -> ()
       | _ -> assert false
   in
   let () = assert (Cache.length c = 1) in

   let () = Cache.remove c 1 in
   let () = assert (Cache.length c = 0) in

   (* Third phase: test cancelation when replacing and removing *)

   let (p1, _) = Lwt.task () in
   let (p2, _) = Lwt.task () in
   let () = Cache.put c 2 p1 in
   let () = assert (Cache.length c = 1) in

   let () = Cache.put c 2 p2 in
   let () = assert (Cache.length c = 1) in
   let () =
      match Lwt.state p1 with
      | Lwt.Fail Lwt.Canceled -> ()
      | _ -> assert false
   in

   let () = Cache.remove c 2 in
   let () = assert (Cache.length c = 0) in
   let () =
      match Lwt.state p2 with
      | Lwt.Fail Lwt.Canceled -> ()
      | _ -> assert false
   in

   ()

let test (module R: Aches.Rache.TRANSFER with type key = int) =
  let module C = Aches_lwt.Lache.Make(R) in
  test (module C : Aches_lwt.Lache.MAP with type key = int)


let test_precise (module Cache: Aches_lwt.Lache.MAP with type key = int) =
   let c = Cache.create 5 in
   let () = assert (Cache.length c = 0) in
   let () = assert (Cache.take c 0 = None) in

   (* Fourth phase: test overflow (can only test when precise because of exact
      number tracking) *)

   let (p1, _) = Lwt.task () in
   let () = Cache.put c 1 p1 in

   let (p2, _) = Lwt.task () in
   let () = Cache.put c 2 p2 in

   let (p3, _) = Lwt.task () in
   let () = Cache.put c 3 p3 in

   let (p4, _) = Lwt.task () in
   let () = Cache.put c 4 p4 in

   let (p5, _) = Lwt.task () in
   let () = Cache.put c 5 p5 in

   let (p6, _) = Lwt.task () in
   let () = Cache.put c 6 p6 in
   let () =
      match Lwt.state p1 with
      | Lwt.Fail Lwt.Canceled -> ()
      | _ -> assert false
   in
   let () = assert (Cache.length c = 5) in

   let p2_taken = Cache.take c 2 in
   let p2_taken = Option.get p2_taken in
   let () = assert (Cache.length c = 4) in

   let (p7, _) = Lwt.task () in
   let () = Cache.put c 7 p7 in
   let () =
      match Lwt.state p2 with
      | Lwt.Sleep -> ()
      | _ -> assert false
   in
   let () =
      match Lwt.state p3 with
      | Lwt.Sleep -> ()
      | _ -> assert false
   in
   let () = assert (Cache.length c = 5) in

   let () = Cache.put c 2 p2_taken in
   let () =
      match Lwt.state p2 with
      | Lwt.Sleep -> ()
      | _ -> assert false
   in
   let () =
      match Lwt.state p3 with
      | Lwt.Fail Lwt.Canceled -> ()
      | _ -> assert false
   in

   let () = Cache.filter c (fun k -> k <> 5) in
   let () =
      match Lwt.state p5 with
      | Lwt.Fail Lwt.Canceled -> ()
      | _ -> assert false
   in

   let ps = Cache.take_all c in
   let () = assert (List.length ps = 4) in
   let () = assert (Cache.length c = 0) in

   ()

let test_precise (module R: Aches.Rache.TRANSFER with type key = int) =
  let module C = Aches_lwt.Lache.Make(R) in
  test_precise (module C : Aches_lwt.Lache.MAP with type key = int)


let test_lru (module Cache: Aches_lwt.Lache.MAP with type key = int) =
   let c = Cache.create 5 in
   let () = assert (Cache.length c = 0) in
   let () = assert (Cache.take c 0 = None) in

   let (p1, _) = Lwt.task () in
   let () = Cache.put c 1 p1 in

   let (p2, _) = Lwt.task () in
   let () = Cache.put c 2 p2 in

   let (p3, _) = Lwt.task () in
   let () = Cache.put c 3 p3 in

   let (p4, _) = Lwt.task () in
   let () = Cache.put c 4 p4 in

   let (p5, _) = Lwt.task () in
   let () = Cache.put c 5 p5 in

   let (p6, _) = Lwt.task () in
   let () = Cache.put c 6 p6 in
   let () =
      match Lwt.state p1 with
      | Lwt.Fail Lwt.Canceled -> ()
      | _ -> assert false
   in
   let () =
      match Lwt.state p2 with
      | Lwt.Sleep -> ()
      | _ -> assert false
   in

   let bor2 = Cache.bind c 2 Lwt.return in
   let bor2 = Option.get bor2 in
   let () =
      match Lwt.state bor2 with
      | Lwt.Sleep -> ()
      | _ -> assert false
   in

   let (p7, _) = Lwt.task () in
   let () = Cache.put c 7 p7 in
   let () =
      match Lwt.state p2 with
      | Lwt.Sleep -> () (* not canceled because lru-refreshed by the bind *)
      | _ -> assert false
   in
   let () =
      match Lwt.state p3 with
      | Lwt.Fail Lwt.Canceled -> () (* canceled because overflow (lru) *)
      | _ -> assert false
   in

   let () = Cache.clear c in
   let () = assert (Cache.length c = 0) in

   ()

let test_lru (module R: Aches.Rache.TRANSFER with type key = int) =
  let module C = Aches_lwt.Lache.Make(R) in
  test_lru (module C : Aches_lwt.Lache.MAP with type key = int)


let test_fifo (module Cache: Aches_lwt.Lache.MAP with type key = int) =
   let c = Cache.create 5 in
   let () = assert (Cache.length c = 0) in
   let () = assert (Cache.take c 0 = None) in

   let (p1, _) = Lwt.task () in
   let () = Cache.put c 1 p1 in

   let (p2, _) = Lwt.task () in
   let () = Cache.put c 2 p2 in

   let (p3, _) = Lwt.task () in
   let () = Cache.put c 3 p3 in

   let (p4, _) = Lwt.task () in
   let () = Cache.put c 4 p4 in

   let (p5, _) = Lwt.task () in
   let () = Cache.put c 5 p5 in

   let (p6, _) = Lwt.task () in
   let () = Cache.put c 6 p6 in
   let () =
      match Lwt.state p1 with
      | Lwt.Fail Lwt.Canceled -> ()
      | _ -> assert false
   in
   let () =
      match Lwt.state p2 with
      | Lwt.Sleep -> ()
      | _ -> assert false
   in

   let bor2 = Cache.bind c 2 Lwt.return in
   let bor2 = Option.get bor2 in
   let () =
      match Lwt.state bor2 with
      | Lwt.Sleep -> ()
      | _ -> assert false
   in

   let (p7, _) = Lwt.task () in
   let () = Cache.put c 7 p7 in
   let () =
      match Lwt.state p2 with
      | Lwt.Sleep -> () (* Not canceled because bind *)
      | _ -> assert false
   in
   let p2 = Cache.bind c 2 Lwt.return in
   let () = assert (p2 = None) in (* Removed because overflow (in fifo) *)
   let () =
      match Lwt.state p3 with
      | Lwt.Sleep -> ()
      | _ -> assert false
   in

   let () = Cache.clear c in
   let () = assert (Cache.length c = 0) in

   ()

let test_fifo (module R: Aches.Rache.TRANSFER with type key = int) =
  let module C = Aches_lwt.Lache.Make(R) in
  test_fifo (module C : Aches_lwt.Lache.MAP with type key = int)


open Aches.Rache
let () = test (module Transfer(LRU)(H))
let () = test (module Transfer(FIFO)(H))

let () = test_precise (module Transfer(LRU)(H))
let () = test_precise (module Transfer(FIFO)(H))

let () = test_lru (module Transfer(LRU)(H))
let () = test_fifo (module Transfer(FIFO)(H))

