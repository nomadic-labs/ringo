(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2021 Nomadic Labs, <contact@nomadic-labs.com>               *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

module H : Hashtbl.HashedType with type t = int = struct
   type t = int
   let equal = (=)
   let hash = Hashtbl.hash
end

let test_em (module EM : Vache.MAP with type key = H.t) =
   let c = EM.create (-1) in
   let () = assert (EM.length c = 0) in
   let () = assert (EM.capacity c = 0) in
   let () = assert (EM.find_opt c 0 = None) in
   let () = EM.replace c 0 "zero" in
   let () = assert (EM.length c = 0) in
   let () = assert (EM.find_opt c 0 = None) in
   ()
let () = test_em (module Vache.EmptyMap(H))

let test_sm (module SM : Vache.MAP with type key = H.t) =
   let c = SM.create (-1) in
   let () = assert (SM.length c = 0) in
   let () = assert (SM.capacity c = 1) in
   let () = assert (SM.find_opt c 0 = None) in
   let () = SM.replace c 0 "zero" in
   let () = assert (SM.length c = 1) in
   let () = assert (SM.find_opt c 0 = Some "zero") in
   let () = SM.replace c 0 "ziro" in
   let () = assert (SM.length c = 1) in
   let () = assert (SM.find_opt c 0 = Some "ziro") in
   let () = SM.replace c 1 "one" in
   let () = assert (SM.length c = 1) in
   let () = assert (SM.find_opt c 1 = Some "one") in
   let () = assert (SM.find_opt c 0 = None) in
   ()
let () = test_sm (module Vache.SingletonMap(H))
let test_m (module V: Vache.MAP with type key = H.t) =
   let module M = struct include V let create _ = create 1 end in
  test_sm (module M)
let () = test_m (module Vache.Map (Vache.LRU_Sloppy) (Vache.Strong) (H))
let () = test_m (module Vache.Map (Vache.LRU_Sloppy) (Vache.Weak) (H))
let () = test_m (module Vache.Map (Vache.LRU_Precise) (Vache.Strong) (H))
let () = test_m (module Vache.Map (Vache.LRU_Precise) (Vache.Weak) (H))
let () = test_m (module Vache.Map (Vache.FIFO_Sloppy) (Vache.Strong) (H))
let () = test_m (module Vache.Map (Vache.FIFO_Sloppy) (Vache.Weak) (H))
let () = test_m (module Vache.Map (Vache.FIFO_Precise) (Vache.Strong) (H))
let () = test_m (module Vache.Map (Vache.FIFO_Precise) (Vache.Weak) (H))

let test_es (module ES : Vache.SET with type elt = H.t) =
   let c = ES.create (-1) in
   let () = assert (ES.length c = 0) in
   let () = assert (ES.capacity c = 0) in
   let () = assert (not (ES.mem c 0)) in
   let () = ES.add c 0 in
   let () = assert (ES.length c = 0) in
   let () = assert (not (ES.mem c 0)) in
   ()
let () = test_es (module Vache.EmptySet(H))

let test_ss (module SS : Vache.SET with type elt = H.t) =
   let c = SS.create (-1) in
   let () = assert (SS.length c = 0) in
   let () = assert (SS.capacity c = 1) in
   let () = assert (not (SS.mem c 0)) in
   let () = SS.add c 0 in
   let () = assert (SS.length c = 1) in
   let () = assert (SS.mem c 0) in
   let () = SS.add c 1 in
   let () = assert (SS.length c = 1) in
   let () = assert (SS.mem c 1) in
   let () = assert (not (SS.mem c 0)) in
   ()
let () = test_ss (module Vache.SingletonSet(H))
let test_s (module V: Vache.SET with type elt = H.t) =
   let module M = struct include V let create _ = create 1 end in
  test_ss (module M)
let () = test_s (module Vache.Set (Vache.LRU_Sloppy) (Vache.Strong) (H))
let () = test_s (module Vache.Set (Vache.LRU_Sloppy) (Vache.Weak) (H))
let () = test_s (module Vache.Set (Vache.LRU_Precise) (Vache.Strong) (H))
let () = test_s (module Vache.Set (Vache.LRU_Precise) (Vache.Weak) (H))
let () = test_s (module Vache.Set (Vache.FIFO_Sloppy) (Vache.Strong) (H))
let () = test_s (module Vache.Set (Vache.FIFO_Sloppy) (Vache.Weak) (H))
let () = test_s (module Vache.Set (Vache.FIFO_Precise) (Vache.Strong) (H))
let () = test_s (module Vache.Set (Vache.FIFO_Precise) (Vache.Weak) (H))
