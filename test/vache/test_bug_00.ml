(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2021 Nomadic Labs, <contact@nomadic-labs.com>               *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

type box = int
module H : Hashtbl.HashedType with type t = box = struct
   type t = box
   let equal = (=)
   let hash = Hashtbl.hash
end
let zero = 0

let test (module Cache: Vache.MAP with type key = box) =
   let cache = Cache.create 100 in
   let () = Cache.replace cache zero () in
   let () = assert (Option.is_some @@ Cache.find_opt cache zero) in
   let () = Gc.minor () in
   let () = assert (Option.is_some @@ Cache.find_opt cache zero) in
   let () = Gc.major () in
   let () = assert (Option.is_some @@ Cache.find_opt cache zero) in
   (* This call to full-major used to make the test fail. Meaning the full-major
      would collect cache elements even though they would be no reason to.
      This bug seems to have been fixed during the rewriting into aches or in
      the ephemeron simplification. We are leaving the test as a regression
      test. *)
   let () = Gc.full_major () in
   let () = assert (Option.is_some @@ Cache.find_opt cache zero) in
   ()

let () = test (module Vache.Map (Vache.LRU_Sloppy) (Vache.Strong) (H))
let () = test (module Vache.Map (Vache.LRU_Precise) (Vache.Strong) (H))
let () = test (module Vache.Map (Vache.FIFO_Sloppy) (Vache.Strong) (H))
let () = test (module Vache.Map (Vache.FIFO_Precise) (Vache.Strong) (H))

(* The test used to fail for weak caches specifically *)
let () = test (module Vache.Map (Vache.LRU_Sloppy) (Vache.Weak) (H))
let () = test (module Vache.Map (Vache.LRU_Precise) (Vache.Weak) (H))
let () = test (module Vache.Map (Vache.FIFO_Sloppy) (Vache.Weak) (H))
let () = test (module Vache.Map (Vache.FIFO_Precise) (Vache.Weak) (H))
