(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2022 Nomadic Labs, <contact@nomadic-labs.com>               *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

module H : Hashtbl.HashedType with type t = int = struct
   type t = int
   let equal = (=)
   let hash = Hashtbl.hash
end
module Resource
: sig
  type t = string ref
  val make : int -> t
  val destroy : int -> t -> unit (* includes a check for single destruction *)
  val check_all_destroyed : unit -> unit
end
= struct
  type t = string ref
  let destroyed = "destroyed"
  let repository = ref []
  let make k =
    let v = string_of_int k in
    let r = ref v in
    repository := r :: !repository;
    r
  let destroy k r =
    assert (!r = string_of_int k);
    r := destroyed
  let check_all_destroyed () =
    assert (List.for_all (fun r -> !r = destroyed) !repository)
end

let test (module Cache: Rache.BORROW with type key = int) =
   let c = Cache.create Resource.destroy 5 in
   let () = assert (Cache.length c = 0) in
   let () = assert (Cache.borrow c 0 (fun _ -> assert false) = None) in

   let () = Cache.borrow_or_make c 0 Resource.make (fun r -> assert (!r = "0")) in
   let () = assert (Cache.length c = 1) in
   let () = assert (Cache.borrow c 0 (fun r -> assert (!r = "0")) = Some ()) in
   let () = assert (Cache.length c = 1) in

   let () = Cache.borrow_or_make c 1 Resource.make (fun r -> assert (!r = "1")) in
   let () = assert (Cache.length c = 2) in
   let () = assert (Cache.borrow c 1 (fun r -> assert (!r = "1")) = Some ()) in
   let () = assert (Cache.borrow c 0 (fun r -> assert (!r = "0")) = Some ()) in

   let () = Cache.remove c 1 in
   let () = assert (Cache.length c = 1) in
   let () = assert (Cache.borrow c 0 (fun r -> assert (!r = "0")) = Some ()) in
   let () = assert (Cache.borrow c 1 (fun _ -> assert false) = None) in

   let () = Cache.clear c in
   let () = assert (Cache.length c = 0) in

   let () = Resource.check_all_destroyed () in

   let () = Cache.borrow_or_make c 10 Resource.make (fun r -> assert (!r = "10")) in
   let () = Cache.borrow_or_make c 11 Resource.make (fun r -> assert (!r = "11")) in
   let () = Cache.borrow_or_make c 12 Resource.make (fun r -> assert (!r = "12")) in
   let () = Cache.borrow_or_make c 13 Resource.make (fun r -> assert (!r = "13")) in
   let () = Cache.borrow_or_make c 14 Resource.make (fun r -> assert (!r = "14")) in
   let () = Cache.borrow_or_make c 15 Resource.make (fun r -> assert (!r = "15")) in
   let () = Cache.borrow_or_make c 16 Resource.make (fun r -> assert (!r = "16")) in
   let () = Cache.borrow_or_make c 17 Resource.make (fun r -> assert (!r = "17")) in
   let () = Cache.borrow_or_make c 18 Resource.make (fun r -> assert (!r = "18")) in
   let () = Cache.borrow_or_make c 19 Resource.make (fun r -> assert (!r = "19")) in

   let () = assert (Cache.length c = 5) in
   let () = Cache.borrow_or_make c 19 Resource.make (fun r -> assert (!r = "19")) in

   let () =
     assert (
       (Cache.fold
         (fun k v inputs ->
           match inputs with
           | [] -> assert false
           | (kk, vv) :: inputs -> assert (k = kk); assert (!v = vv); inputs)
         c
         [ (19, "19"); (18, "18"); (17, "17"); (16, "16"); (15, "15") ])
       = [])
   in

   let () = Cache.remove c 15 in
   let () = Cache.remove c 17 in
   let () = Cache.remove c 16 in
   let () = Cache.remove c 19 in
   let () = Cache.remove c 18 in

   let () = assert (Cache.length c = 0) in
   let () = Resource.check_all_destroyed () in

   ()

let () = test (module Rache__Functors.Borrow (Ringo.LRU_Collection) (H))
let () = test (module Rache.Borrow (Rache.LRU) (H))
let () = test (module Rache__Functors.Borrow (Ringo.FIFO_Sloppy_Collection) (H))
let () = test (module Rache__Functors.Borrow (Ringo.FIFO_Precise_Collection) (H))
let () = test (module Rache.Borrow (Rache.FIFO) (H))

let test_empty () =
   let module Cache = Rache.EmptyBorrowMap (H) in
   let c = Cache.create Resource.destroy (-1) in
   let () = assert (Cache.length c = 0) in
   let () = assert (Cache.borrow c 0 (fun _ -> assert false) = None) in
   let () = Cache.borrow_or_make c 0 Resource.make (fun r -> assert (!r = "0")) in
   let () = Resource.check_all_destroyed () in
   let () = assert (Cache.length c = 0) in
   let () = assert (Cache.borrow c 0 (fun _ -> assert false) = None) in
   ()
let () = test_empty ()

let test_single () =
   let module Cache = Rache.SingletonBorrowMap (H) in
   let c = Cache.create Resource.destroy (-1) in
   let () = assert (Cache.length c = 0) in
   let () = assert (Cache.borrow c 0 (fun _ -> assert false) = None) in
   let () = Cache.borrow_or_make c 0 Resource.make (fun r -> assert (!r = "0")) in
   let () = assert (Cache.length c = 1) in
   let () = assert (Cache.borrow c 0 (fun r -> assert (!r = "0")) = Some ()) in
   let () = Cache.remove c 0 in
   let () = Resource.check_all_destroyed () in
   ()
let () = test_single ()
